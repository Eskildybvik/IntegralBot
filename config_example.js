module.exports = {
	token: "",
	removalEmoji: "❌",
	removalCount: 3,
	removalText: true,
	removalTexts: [
		"[usr] posted something that was downvoted."
	]
}