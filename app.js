var eris = require("eris");
var config = require("./config");

var bot = new eris(config.token);
bot.on("ready", () => {
	console.log("Connected");
});

bot.on("messageCreate", (msg) => {
	if (msg.content === "Is bot exist?") {
		bot.createMessage(msg.channel.id, "Yes");
	}
});

bot.on("messageReactionAdd", (msg, emoji, emojiID, emojiName, userID) => {
	bot.getMessage(msg.channel.id, msg.id).then((message) => {
		if (message.reactions[config.removalEmoji].count >= config.removalCount) {
			message.delete();
			if (config.removalText) {
				bot.createMessage(msg.channel.id, {
					embed: {
						title: "Message removed",
						description: config.removalTexts[Math.floor(Math.random()*config.removalTexts.length)].replace("[usr]", message.author.mention),
						color: 16711680
					}
				});
			}
		}
	});
});

bot.connect();